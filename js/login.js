'use strict';

jQuery(function ($) {
    $(document).ready(function () {
        //-------------------------------- TABS -------------------------------

        $('.login-tabs-list li').click(function () {
            let tab_id = $(this).attr('data-tab');

            $('.login-tabs-list li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $("#" + tab_id).addClass('current');
        })

        //------------------------------- POP-UP`s -----------------------------

    });
});


