
$(document).ready(function () {
    //-------------------------------- Dropdowns form -------------------------------
    function formDropdowns(id, list, item, select, block, dropdown) {

        $(id).click(function () {
            $(list).slideToggle(150);
            $(this).find('span').toggleClass('active-dropdown');
        });
        $(item).click(function () {
            $(select).val($(this).text());
        });

        $(document).on("click", function (event) {
            let $trigger = $(dropdown);
            if ($trigger !== event.target && !$trigger.has(event.target).length) {
                $(block).slideUp(150);
            }
        });
    }

    formDropdowns('#cities-list', ".cities-list", '.city-item', '.city-select', '.cities-list', ".cities-dropdown");
    formDropdowns('#streets-list', ".streets-list", '.street-item', '.street-select', '.streets-list', ".streets-dropdown");
    formDropdowns('#novaposhta-cities-list', ".novaposhta-cities-list", '.novaposhta-city-item', '.novaposhta-city-select', '.novaposhta-cities-list', ".novaposhta-cities-dropdown");
    formDropdowns('#ukrposhta-cities-list', ".ukrposhta-cities-list", '.ukrposhta-city-item', '.ukrposhta-city-select', '.ukrposhta-cities-list', ".ukrposhta-cities-dropdown");
    formDropdowns('#novaposta-branch-list', ".novaposta-branch-list", '.novaposta-branch-item', '.novaposta-branch-select', '.novaposta-branch-list', ".novaposta-branch-dropdown");
    formDropdowns('#novaposta-courier-list', ".novaposta-courier-list", '.novaposta-courier-item', '.novaposta-courier-select', '.novaposta-courier-list', ".novaposta-courier-dropdown");
    formDropdowns('#ukrposhta-branch-list', ".ukrposhta-branch-list", '.ukrposhta-branch-item', '.ukrposhta-branch-select', '.ukrposhta-branch-list', ".ukrposhta-branch-dropdown");


    //  --------------------------Pay method-------

    $('.pay-head-visa').on('click', function () {
        $('.visa-item').addClass('selected-input');
        $('.visa-card').slideDown();
        $('.liqpay-item').removeClass('selected-input');
        $('.liqpay-card').slideUp();
        $('.cash-item').removeClass('selected-input');
    });

    $('.pay-head-liqpay').on('click', function () {
        $('.liqpay-item').addClass('selected-input');
        $('.liqpay-card').slideDown();
        $('.visa-item').removeClass('selected-input');
        $('.visa-card').slideUp();
        $('.cash-item').removeClass('selected-input');
    });

    $('.cash-head').on('click', function () {
        $('.cash-item').addClass('selected-input');
        $('.liqpay-item').removeClass('selected-input');
        $('.liqpay-card').slideUp();
        $('.visa-item').removeClass('selected-input');
        $('.visa-card').slideUp();
    });

    //  --------------------------Delivery method-------

    $('.ukrposhta-head').on('click', function () {
        $('.ukrposhta-item').addClass('selected-input');
        $('.ukrposhta-content').slideDown();
        $('.novaposhta-item').removeClass('selected-input');
        $('.novaposhta-content').slideUp();
    });

    $('.novaposhta-head').on('click', function () {
        $('.ukrposhta-item').removeClass('selected-input');
        $('.ukrposhta-content').slideUp();
        $('.novaposhta-item').addClass('selected-input');
        $('.novaposhta-content').slideDown();
    });


    // --------------mobile order hide show-----------
    
    if ($(window).width() <= 992) {
		$('.order-show-mobile').on('click', function() {
            $('.order-content-wrap').slideToggle();
            $(this).find('.icon-angle-right').toggleClass('active-dropdown');
        });
    
        $('.promo-show-mobile').on('click', function() {
            $('.promo-input-wrap').slideToggle();
            $(this).find('.icon-angle-right').toggleClass('active-dropdown');
            // $(this).toggleClass('border-none');
        });
	 }

    

});