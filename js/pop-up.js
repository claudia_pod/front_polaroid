'use strict';

jQuery(function ($) {
    $(document).ready(function () {

        //------------------------------- POP-UP`s -----------------------------
        let confirmation = $('.confirmation'),
            description = $('.description'),
            colorSelection = $('.color-selection'),
            catalogFilter = $('.catalog-filter'),
            contact = $('.contact'),
            popUps = [confirmation, description, colorSelection, catalogFilter, contact];

        jQuery.each(popUps, function () {
            $(this).on("click", function(e) {
                e.preventDefault();
                //------------------------------- Find pop-up -----------------------------
                let linkName =  $(this).attr('class'),
                    pop = $('.' + linkName + '-pop-up');

                //------------------------------- Show pop-up -----------------------------
                pop.fadeIn(300, function () {
                    $(this).focus();
                });
                //---------------------- Disable body scroll -------------------------------
                $('body').addClass('body-scroll-off');
                //---------------------- Close on click out pop-up ------------------------
                $(document).on('click', function(e) {
                    if ($(e.target).is(pop)) {
                        pop.css('display', 'none');
                        $('body').removeClass('body-scroll-off');
                    }
                });

                //---------------------- Close on click X btn ------------------------------
                $(".close-x").on('click', function() {
                    pop.css('display', 'none')
                    $('body').removeClass('body-scroll-off');
                })
            })
        })
    });

    $(document).ready(function () {

        //------------------------------- POP-UP`s -----------------------------
        let discount_1 = $('.discount_1'),
            discount_2 = $('.discount_2'),
            discount_3 = $('.discount_3'),
            personal = $('.personal'),
            popUps = [discount_1, discount_2, discount_3, personal];

        jQuery.each(popUps, function () {
            $(this).on("click", function(e) {
                e.preventDefault();
                //------------------------------- Find pop-up -----------------------------
                let linkName =  $(this).attr('class'),
                    pop = $('.' + linkName + '-pop-up');

                //------------------------------- Show pop-up -----------------------------
                pop.addClass('opened');
                //---------------------- Disable body scroll -------------------------------
                $('body').addClass('body-scroll-off');
                //---------------------- Close on click out pop-up ------------------------
                $(document).on('click', function(e) {
                    if ($(e.target).is(pop)) {
                        pop.removeClass('opened');
                        $('body').removeClass('body-scroll-off');
                    }
                });

                //---------------------- Close on click X btn ------------------------------
                $(".close-x").on('click', function() {
                    pop.removeClass('opened');
                    $('body').removeClass('body-scroll-off');
                })
            })
        })
    });

    // ----------------Video pop up------------

    $('.video-btn').on('click', function() {
        $('.video-modal-wrap').fadeIn();
        $('body').addClass('body-scroll-off');
    })

    $('.video-modal-wrap').on('click', function() {
        $('.video-modal-wrap').fadeOut();
        $('body').removeClass('body-scroll-off');
        let stopVideo = $('.iframe iframe').attr("src");
        $('.iframe iframe').attr("src", stopVideo);
    })

});