'use strict';

jQuery(function ($) {
    $(document).ready(function () {
        //-------------------------------- Dropdown sort -------------------------------
        $("#options").click(function() {
            $(".sub_menu").slideToggle(150);
            $('.dropdown-title img').toggleClass('active-dropdown');
        });
        $(".btn-filter").click(function() {
            $(".selected").text($(this).text());
        });

        $(document).on("click", function (event) {
            let $trigger = $(".sort-dropdown-block");
            if ($trigger !== event.target && !$trigger.has(event.target).length) {
                $(".sub_menu").slideUp(150);
                $('.dropdown-title img').removeClass('active-dropdown');
            }
        });

        //-------------------------------- catalog hover slider -------------------
        $('.news-img-hover').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.news-color-slider'
        });

        $('.news-color-slider').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: false,
            asNavFor: '.news-img-hover',
            focusOnSelect: true,
            prevArrow: "<button type='button' class='slick-prev top-slider-prev'><img src='img/icons/col-left.svg' alt='Arrow icon'></button>",
            nextArrow: "<button type='button' class='slick-next top-slider-next'><img src='img/icons/col-right.svg' alt='Arrow icon'></i></button>",
        });
        //-------------------------------- isotop selector ------------------------
        let $grid = $('.catalog-list').isotope({
            itemSelector: '.news-item',
            layoutMode: 'fitRows',
            getSortData: {
                type: '.type',
                price: '.price'
            }
        });
        $('.sub_menu').on( 'click', 'button', function() {
            let filterValue = $( this ).attr('data-filter');
            $grid.isotope({ filter: filterValue });
        });
        $('.sub_menu').on( 'click', 'button', function() {
            let sortByValue = $(this).attr('data-sort-by'),
                expensiveFirst = $(this).attr('data-expensive');
            if (expensiveFirst == "expensive") {
                $grid.isotope({ sortBy: sortByValue, sortAscending: false});
            } else {
                $grid.isotope({ sortBy: sortByValue, sortAscending: true });
            }

        });
        $('.sub_menu').each( function( i, buttonGroup ) {
            let $buttonGroup = $( buttonGroup );
            $buttonGroup.on( 'click', 'li', function() {
                $buttonGroup.find('.active-btn-filter').removeClass('active-btn-filter');
                $( this ).addClass('active-btn-filter');
            });
        });

        //-------------------------------- Like btn ------------------------
        $('.like-btn').click( function() {
            $('.like-btn').toggleClass('like-btn-active')
        })
        //------------------------------ FAQ accordion -----------------------------------

        $(".accordion-content:eq(0)").show();
        $(".accordion-content:eq(1)").show();
        $(".accordion-content:eq(2)").show();
        $('.accordion-arrow:eq(0)').toggleClass('accordion-rotate');
        $('.accordion-arrow:eq(1)').toggleClass('accordion-rotate');
        $('.accordion-arrow:eq(2)').toggleClass('accordion-rotate');
        $('.accordion-arrow').toggleClass('accordion-rotate');

        $(".accordion-title").on("click", function(e) {
            e.preventDefault();
            let $this = $(this);

            $this.toggleClass("accordion-active");
            $this.next().slideToggle();
            $('.accordion-arrow',this).toggleClass('accordion-rotate');
        });
    });
});