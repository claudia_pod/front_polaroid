

// --------------------------Header slider-----------------------------

$(document).ready(function () {
	$('.header-slider').slick({
		infinite: true,
		autoplay: true,
		autoplaySpeed: 7000,
		prevArrow: "<button type='button' class='slick-prev top-slider-prev'><img src='/img/icons/arrow-left.svg' alt='Arrow icon'></button>",
		nextArrow: "<button type='button' class='slick-next top-slider-next'><img src='/img/icons/arrow-right.svg' alt='Arrow icon'></button>",
	});

	// --------------show search block-----

	$('.show-search-btn').on('click', function() {
		$('.show-search-btn .search-icon, .main-nav').toggleClass('d-none');
		$('.search-block').toggleClass('show-search-block');
		$(".search-input").focus();

	});

	// ------Mobile menu-------------

	$('.menu-btn').on('click', function () {
		$('.hamburger').toggleClass('hamburger_active');
		$('.main-nav').toggleClass('mob-nav-active');
		$('body').toggleClass('hidden');
	});

	// --------------------Show sub-menu mobile--------
	
	$('.sunglasses-item .arrow').on('click', function() {
		$('.sunglasses-submenu').toggleClass('d-flex');
		$(this).toggleClass('toggle-arrow');
	});

	$('.optic-item .arrow').on('click', function() {
		$('.optic-submenu').toggleClass('d-flex');
		$(this).toggleClass('toggle-arrow');
	});

	// ----------------------Slider product hover---------------------------------

	$('.new-item-footer').mouseenter( function() {
		$(this).closest('.news-item').find('.news-img-hover').css('opacity', '1');
		$(this).closest('.news-item').find('.img-wrap').css('opacity', '0');
	});

	$('.new-item-footer').mouseleave( function() {
		$(this).closest('.news-item').find('.news-img-hover').css('opacity', '0');
		$(this).closest('.news-item').find('.img-wrap').css('opacity', '1');
	});

	// ----------------------Slider product change img-------------------------

	// $('.color-slider-item img').on('click', function() {
	// 	let colorImgSrc = $(this).attr('src');
	// 	$(this).closest('.news-item').find('.news-img').attr('src', colorImgSrc);
	// });

	$('.news-color-slider').on('click', function() {
		let colorImgSrc = $(this).find('.slick-current img').attr('src');
		$(this).closest('.news-item').find('.news-img').attr('src', colorImgSrc);
	});

	// ------------------Cookies pop up -----------------

	$('.cookies__close, .cookies__agree').on('click', function() {
		$('.cookies-wrap').fadeOut();
	});
	
});

// -------------------------Error input -----------------

jQuery(document).ready(function ($) {
	if ($(".input-error")) {
		$(".input-error").before( "<p class='error-message'>Email адрес введен не верно</p>" );
	}
});

