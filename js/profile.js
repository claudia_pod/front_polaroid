'use strict';

$(document).ready(function () {

    // --------------------Copy bonus--------------

    $('#bonus-copy-btn').on('click', function () {
        $('#bonus-input').select();
        document.execCommand("copy");
    });


      //-------------------------------- Dropdowns form -------------------------------
      function formDropdowns(id, list, item, select, block, dropdown) {

        $(id).click(function () {
            $(list).slideToggle(150);
            $(this).find('span').toggleClass('active-dropdown');
        });
        $(item).click(function () {
            $(select).val($(this).text());
        });

        $(document).on("click", function (event) {
            let $trigger = $(dropdown);
            if ($trigger !== event.target && !$trigger.has(event.target).length) {
                $(block).slideUp(150);
                $(id).find('span').removeClass('active-dropdown');
            }

        });
    }

    formDropdowns('#cities-list', ".cities-list", '.city-item', '.city-select', '.cities-list', ".cities-dropdown");
    formDropdowns('#streets-list', ".streets-list", '.street-item', '.street-select', '.streets-list', ".streets-dropdown");
    formDropdowns('#сountry-list', ".сountry-list", '.сountry-item', '.сountry-select', '.сountry-list', ".сountry-dropdown");
    formDropdowns('#year-list', ".year-list", '.year-item', '.year-select', '.year-list', ".year-dropdown");
    formDropdowns('#month-list', ".month-list", '.month-item', '.month-select', '.month-list', ".month-dropdown");
    formDropdowns('#day-list', ".day-list", '.day-item', '.day-select', '.day-list', ".day-dropdown");
    formDropdowns('#gender-list', ".gender-list", '.gender-item', '.gender-select', '.gender-list', ".gender-dropdown");


      //-------------------------------- Favorite hover slider -------------------

      $('.news-img-hover').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        swipe: false,
		draggable: false,
    });

    $('.news-color-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: false,
        swipe: false,
		draggable: false,
        asNavFor: '.news-img-hover',
        focusOnSelect: true,
        prevArrow: "<button type='button' class='slick-prev top-slider-prev'><span class='icon-angle-left color-slider-arrow'></span></button>",
        nextArrow: "<button type='button' class='slick-next top-slider-next'><span class='icon-angle-right color-slider-arrow'></span></button>",
        responsive: [{
            breakpoint: 768,
            settings: "unslick"
        }]
    });

     // --------------mobile orders hide show-----------

     if ($(window).width() <= 992) {

		$('.table-orders-body .tr').on('click', function() {
            $(this).find('.info-mob-wrap').slideToggle();
            $(this).find('.icon-angle-right').toggleClass('active-dropdown');
        });

        $('.promo-show-mobile').on('click', function() {
            $('.promo-input-wrap').slideToggle();
            $(this).find('.icon-angle-right').toggleClass('active-dropdown');
            $(this).toggleClass('border-none');
        });
	 }
});