
$(document).ready(function () {

	// ----------------Main Banner slider------------------------

	$('.main-banner').on('init', function (event, slick, direction) {

		if (!($('.main-banner .slick-slide').length > 1)) {

			$('.main-banner .slick-dots').hide();
		}
	});


	$('.main-banner').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 7000,
		dots: true,
		arrows: false,
		customPaging: function (slider, i) {
			return '<div class="dots-item" id=' + i + "></div>";
		},

	});


	// --------------------------------News & hits sliders-----------------------


	$('.news-slider').on('init', function (event, slick, direction) {

		if (!($('.news-slider .slick-slide').length > 1)) {

			$('.news-slider .slick-dots').hide();
			$('.news-slider .slick-track').css('margin', '0');
		}
	});

	$('.news-slider').slick({
		slidesToShow: 3,
		slidesToScroll: 3,
		infinite: true,
		autoplay: false,
		autoplaySpeed: 7000,
		useTransform: true,
		dots: false,
		draggable: false,
		arrows: true,
		centerMode: false,
		prevArrow: "<button type='button' class='slick-prev news-slider-prev'><img src='/img/icons/arrow-left.svg' alt='Arrow icon'></button>",
		nextArrow: "<button type='button' class='slick-next news-slider-next'><img src='/img/icons/arrow-right.svg' alt='Arrow icon'></button>",
		customPaging: function (slider, i) {
			return '<div class="news-dots" id=' + i + "></div>";
		},

		responsive: [

			{
				breakpoint: 992,
				settings: {
					draggable: true,
					slidesToShow: 2,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 768,
				settings: {
					draggable: true,
					slidesToShow: 2,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 576,
				settings: {
					draggable: true,
					dots: false,
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			},
		]


	});


	$('.news-slider .news-img-hover').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		swipe: false,
		draggable: false,
		fade: true,
	});

	$('.news-slider .news-color-slider').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: false,
		draggable: false,
		swipe: false,
		asNavFor: '.news-slider .news-img-hover',
		focusOnSelect: true,
		prevArrow: "<button type='button' class='slick-prev top-slider-prev'><span class='icon-angle-left color-slider-arrow'></span></button>",
		nextArrow: "<button type='button' class='slick-next top-slider-next'><span class='icon-angle-right color-slider-arrow'></span></button>",
	});

});