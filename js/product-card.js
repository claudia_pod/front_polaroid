'use strict';

jQuery(function ($) {
    $(document).ready(function () {

        //-------------------------------- PRODUCT SLIDER -------------------------------
        $('.slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            fade: true,
            infinite: false,
            customPaging: function (slider, i) {
                let imgAddress = $(slider.$slides[i]).attr('src');
                return '<img class="tab" src="' + imgAddress + '" alt="Product img">';

            },
        });
        //------------------------------ FAQ accordion -----------------------------------

        $(".accordion-content").show();
        $('.accordion-arrow').toggleClass('accordion-rotate');

        $(".accordion-title").on("click", function (e) {
            e.preventDefault();
            let $this = $(this);

            $this.toggleClass("accordion-active");
            $this.next().slideToggle();
            $('.accordion-arrow', this).toggleClass('accordion-rotate');
        });
        //-------------------------------- Responses slider -------------------------------
        $('.responses-slider').slick({
            prevArrow: "<button id='prev' type='button' class='slider-btn comment-slider-prev'><span class='icon-angle-left slider-arrow'></span></button>",
            nextArrow: "<button id='next' type='button' class='slider-btn comment-slider-next'><span class='icon-angle-right slider-arrow'></span></button>",
        });
        //-------------------------------- Rating stars -------------------------------
        $("label").click(function () {
            $(this).parent().find("label").css({
                "color": "#E0E0E0"
            });
            $(this).css({
                "color": "#FFB800"
            });
            $(this).nextAll().css({
                "color": "#FFB800"
            });
        });
        // --------------------------------News & hits sliders-----------------------


        $('.news-slider').on('init', function (event, slick, direction) {

            if (!($('.news-slider .slick-slide').length > 1)) {

                $('.news-slider .slick-dots').hide();
                $('.news-slider .slick-track').css('margin', '0');
            }
        });

        $('.news-slider').slick({
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            autoplay: false,
            autoplaySpeed: 7000,
            dots: false,
            draggable: false,
            arrows: false,
            prevArrow: "<button id='prev' type='button' class='slider-btn comment-slider-prev'><span class='icon-angle-left slider-arrow'></span></button>",
            nextArrow: "<button id='next' type='button' class='slider-btn comment-slider-next'><span class='icon-angle-right slider-arrow'></span></button>",
            customPaging: function (slider, i) {
                return '<div class="news-dots" id=' + i + "></div>";
            },
            responsive: [{
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        arrows: true,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true,
                    }
                },
            ]
        });


        $('.news-img-hover').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            swipe: false,
		    draggable: false,
        });

        $('.news-color-slider').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: false,
            asNavFor: '.news-img-hover',
            focusOnSelect: true,
            swipe: false,
		    draggable: false,
            prevArrow: "<button type='button' class='slick-prev top-slider-prev'><span class='icon-angle-left color-slider-arrow'></span></button>",
            nextArrow: "<button type='button' class='slick-next top-slider-next'><span class='icon-angle-right color-slider-arrow'></span></button>",
        });
        // -------------------------------- Read more btn -----------------------

        $('.read-more-btn').click(function () {
            $('.more-text').slideToggle(500);
            if ($('.read-more-btn').text() == "Читать полностью") {
                $(this).text("Свернуть")
            } else {
                $(this).text("Читать полностью")
            }
        });

        // -------------------------------- Leave comment btn -----------------------
        $('.comment-mobile-btn').click(function () {
            $('.comment-mobile-btn').slideUp({
                duration: 500,
                complete: function() {
                    $('.leave-comment-wrapper').slideDown(500);
                }
            });
        })
        $(document).on("click", function (event) {
            let $trigger = $(".responses-section");
            if ($trigger !== event.target && !$trigger.has(event.target).length) {
                $('.leave-comment-wrapper').slideUp({
                    duration: 500,
                    complete: function() {
                        $('.comment-mobile-btn').slideDown(500);
                    }
                });
            }
        });
        // -------------------------------- Share btn --------------------------------
        $('.share-btn').click(function () {
            $('.share-list').slideToggle(500);
        })
        $(document).on("click", function (event) {
            let $trigger = $(".icons-block-item");
            if ($trigger !== event.target && !$trigger.has(event.target).length) {
                $(".share-list").slideUp(500);
            }
        });
    });
});

$(document).ready(function () {

    //------------------------------- POP-UP`s -----------------------------

    $(".empty-btn").on('click', function(e) {
        e.preventDefault();
        //------------------------------- Find pop-up -----------------------------
        let pop = $('.empty-pop-up');

        //------------------------------- Show pop-up -----------------------------
        pop.addClass('opened');
        //---------------------- Disable body scroll -------------------------------
        $('body').addClass('body-scroll-off');
        //---------------------- Close on click out pop-up ------------------------
        $(document).on('click', function(e) {
            if ($(e.target).is(pop)) {
                pop.removeClass('opened');
                $('body').removeClass('body-scroll-off');
            }
        });

        //---------------------- Close on click X btn ------------------------------
        $(".close-x").on('click', function() {
            pop.removeClass('opened');
            $('body').removeClass('body-scroll-off');
        })
    })

    $(".delivery-block__inner").on('click', function(e) {
        e.preventDefault();
        //------------------------------- Find pop-up -----------------------------
        let pop = $('.delivery-pop-up');

        //------------------------------- Show pop-up -----------------------------
        pop.addClass('opened');
        //---------------------- Disable body scroll -------------------------------
        $('body').addClass('body-scroll-off');
        //---------------------- Close on click out pop-up ------------------------
        $(document).on('click', function(e) {
            if ($(e.target).is(pop)) {
                pop.removeClass('opened');
                $('body').removeClass('body-scroll-off');
            }
        });

        //---------------------- Close on click X btn ------------------------------
        $(".close-x").on('click', function() {
            pop.removeClass('opened');
            $('body').removeClass('body-scroll-off');
        })
    })

    $(".special-offer-block").on('click', function(e) {
        e.preventDefault();
        //------------------------------- Find pop-up -----------------------------
        let pop = $('.special-pop-up');

        //------------------------------- Show pop-up -----------------------------
        pop.addClass('opened');
        //---------------------- Disable body scroll -------------------------------
        $('body').addClass('body-scroll-off');
        //---------------------- Close on click out pop-up ------------------------
        $(document).on('click', function(e) {
            if ($(e.target).is(pop)) {
                pop.removeClass('opened');
                $('body').removeClass('body-scroll-off');
            }
        });

        //---------------------- Close on click X btn ------------------------------
        $(".close-x").on('click', function() {
            pop.removeClass('opened');
            $('body').removeClass('body-scroll-off');
        })
    })
});